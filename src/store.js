import Vue from 'vue'
import Vuex from 'vuex'

import uuidv1 from 'uuid/v1'
import dotenv from 'dotenv'
import io from 'socket.io-client'

dotenv.config()

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        rooms: {},
        username: `user${ uuidv1().split('-').shift() }`,
        selectedRoom: '',
        socket: io(`${ process.env.VUE_APP_WSHOST }:${ process.env.VUE_APP_WSPORT }`)
    },
    getters: {
        roomNames(state) {
            return Object.keys(state.rooms)
        },
        messages(state) {
            if (state.selectedRoom) {
                return state.rooms[state.selectedRoom].messages
            }
        }
    },
    mutations: {
        addMessage(state, message) {
            state.rooms[message.room].messages.push(message)
        },
        createRoom(state, name) {
            Vue.set(state.rooms, name, {messages: []})
        },
        selectRoom(state, room) {
            state.selectedRoom = room
        },
        setUsername(state, username) {
            state.username = username
        }
    },
    actions: {
        broadcastMessage({ commit, state }, message) {
            state.socket.emit('message', message)
        },
        createRoom({ commit, state }, name) {
            commit('createRoom', name)
        },
        selectRoom({ commit, state }, room) {
            commit('selectRoom', room)
        },
        setUsername({ commit, state }, username) {
            commit('setUsername', username)
        }
    }
})
