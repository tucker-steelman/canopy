const server = require('http').createServer()
const io = require('socket.io')(server)
const dotenv = require('dotenv')

dotenv.config()

io.on('connection', client => {
    console.log(`${ client.id } has connected`)

    client.on('message', message => {
        console.log(`${ client.id } (${ message.author }) in ${ message.room } says "${ message.content }"`)
        io.emit('message', message)
    })
})

server.listen(process.env.VUE_APP_WSPORT)